-- Zabbix official partitioning solution prototype
-- Version 0.1
-- Currently not for the production
CREATE TABLE IF NOT EXISTS manage_partitions
(
  tablename VARCHAR(64) NOT NULL,
  period VARCHAR(64) NOT NULL, 
  days_to_keep INT NOT NULL DEFAULT '90',
  days_in_advance INT NOT NULL DEFAULT '30',
  last_updated TIMESTAMP DEFAULT NULL ,
  comments VARCHAR(128) DEFAULT '1' 
)
WITH (
  OIDS=FALSE
);
ALTER TABLE manage_partitions
  OWNER TO zabbix;

INSERT INTO manage_partitions (tablename, period, days_to_keep,days_in_advance, last_updated, comments) VALUES ('history', 'day', 182,7, now(), '');
INSERT INTO manage_partitions (tablename, period, days_to_keep,days_in_advance, last_updated, comments) VALUES ('history_uint', 'day', 182,7, now(), '');
INSERT INTO manage_partitions (tablename, period, days_to_keep,days_in_advance, last_updated, comments) VALUES ('history_str', 'day', 182,7, now(), '');
INSERT INTO manage_partitions (tablename, period, days_to_keep,days_in_advance, last_updated, comments) VALUES ('history_text', 'day', 182,7, now(), '');
INSERT INTO manage_partitions (tablename, period, days_to_keep,days_in_advance, last_updated, comments) VALUES ('history_log', 'day', 182,7, now(), '');
INSERT INTO manage_partitions (tablename, period, days_to_keep,days_in_advance, last_updated, comments) VALUES ('trends', 'month',730,30, now(), '');
INSERT INTO manage_partitions (tablename, period, days_to_keep,days_in_advance, last_updated, comments) VALUES ('trends_uint', 'month', 730,30, now(), '');

CREATE TYPE partition AS (
    name    VARCHAR,
    min_value TIMESTAMP,
    range       VARCHAR
);
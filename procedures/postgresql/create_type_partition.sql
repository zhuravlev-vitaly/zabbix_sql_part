-- Zabbix official partitioning solution prototype
-- Version 0.1
-- Currently not for the production
CREATE TYPE partition AS (
    name    VARCHAR,
    min_value TIMESTAMP,
    range       VARCHAR
);
  CREATE OR REPLACE FUNCTION my_test (VARCHAR,VARCHAR, INT)
RETURNS void AS
$BODY$
DECLARE
  master_table    VARCHAR := $1;
  partition_range VARCHAR := $2;
  future_part_num_required INT := $3;
  
  part RECORD;
  part_array INTEGER ARRAY;
  part_array_timestamp INTEGER;
    --time part
  range_interval  INTEGER := EXTRACT(EPOCH FROM partition_range::INTERVAL);
  now             INTEGER := EXTRACT(EPOCH FROM NOW())::INT;
  future_part_count_record RECORD;
  future_part_num_to_add INT;  

  current_partition VARCHAR;
  current_partition_min INTEGER;
  current_partition_max INTEGER;
  
  current_partition_min_date DATE;
  current_partition_max_date DATE;
  current_if_word VARCHAR := 'IF';

  function_name   VARCHAR := master_table || '_function';
  function_start VARCHAR := FORMAT(
    'CREATE OR REPLACE FUNCTION %s
         RETURNS trigger AS
       $$ 
       BEGIN
         IF TG_OP = ''INSERT'' THEN 
           ',
  function_name);
  function_end VARCHAR := FORMAT(
    '      ELSE
             RETURN NEW;
           END IF;
         END IF; 
         RETURN NULL; 
       END $$
       LANGUAGE plpgsql VOLATILE
       COST 100',
  function_name);
  function_middle VARCHAR := '';
  iterator INTEGER := 0;
  
BEGIN
      	--grab all records (their timestamp is required)
        FOR part IN SELECT substring(c.relname from '\d+$')AS epoch_timestamp 
			FROM pg_inherits 
			JOIN pg_class AS c ON (inhrelid=c.oid) 
			JOIN pg_class as p ON (inhparent=p.oid AND p.relname = master_table)
			ORDER BY  epoch_timestamp DESC

            
            
	-- place results into array part_array
    LOOP
	    part_array[iterator+1] = part.epoch_timestamp;
         
             RAISE NOTICE 'PARTITION %' ,part.epoch_timestamp;
             iterator = iterator +1;
	END LOOP;


    -- find how many 'future' partitions are there:
     SELECT COUNT(*) INTO future_part_count_record FROM (
	SELECT substring(c.relname from '\d+$')AS epoch_timestamp,c.relname AS child, p.relname AS parent
	FROM
	    pg_inherits 
	    JOIN pg_class AS c ON (inhrelid=c.oid)
	    JOIN pg_class AS p ON (inhparent=p.oid
		AND p.relname = master_table
	    )
	      ORDER BY  epoch_timestamp ASC
	) MAIN
    WHERE epoch_timestamp::INT > EXTRACT(EPOCH FROM now())::INT;
    
    future_part_num_to_add = future_part_num_required - future_part_count_record.count;
     RAISE NOTICE 'number of partitions to add: %',future_part_num_to_add;

	-- create future partitions and append their epoch to part_array for function update
	WHILE future_part_num_to_add > 0 LOOP
		RAISE NOTICE 'number of partitions to add left: %',future_part_num_to_add;
	       
		--part_array[0] is max element as array is sorted
		
	current_partition_min = part_array[1] + range_interval;
	current_partition_max = current_partition_min + range_interval;
        --epoch to date:
        current_partition_min_date = to_timestamp(current_partition_min);
        current_partition_max_date = current_partition_min_date +partition_range::INTERVAL;

	RAISE NOTICE 'current_partition_min_date is %',current_partition_min_date;
        current_partition = FORMAT('%s_p%s', master_table, to_timestamp(current_partition_min)::DATE);
		
		PERFORM add_partition(master_table, current_partition, current_partition_min, current_partition_max,'zabbix',TRUE);

		future_part_num_to_add = future_part_num_to_add - 1;
		part_array = current_partition_min || part_array; -- pop new element to array
	END LOOP;
    
	iterator = 0;
	FOREACH part_array_timestamp IN ARRAY part_array LOOP

	--we need to start adding lines from second element so we know both min and max 
	IF iterator > 0 THEN 
		--prepare for insert into trigger
		current_partition_max = part_array_timestamp;
		function_middle = function_middle || FORMAT('%s NEW.clock >= %s AND NEW.clock < %s THEN 
			INSERT INTO %I VALUES (NEW.*);
			',current_if_word,current_partition_min,current_partition_max,current_partition);
		--RAISE NOTICE '%', function_middle;
		current_if_word = 'ELSIF';


	END IF;
		--prepare for next round:
		iterator = iterator +1;
		current_partition_min = part_array_timestamp;
		current_partition = FORMAT('%s_p%s', master_table, to_timestamp(part_array_timestamp)::DATE);
	END LOOP;
	
      RAISE NOTICE 'function code:
       %
       %
       %', function_start,function_middle,function_end;
END
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
 
ALTER FUNCTION my_test(VARCHAR,VARCHAR, INT)
OWNER TO zabbix;

--test
SELECT my_test('history','1 week',4);



DROP FUNCTION find_max (in TIMESTAMP,in VARCHAR,out current_min_value TIMESTAMP, out current_max_value TIMESTAMP);
  CREATE OR REPLACE FUNCTION find_max (in TIMESTAMP,in VARCHAR,out current_min_value TIMESTAMP, out current_max_value TIMESTAMP)
AS
$BODY$
DECLARE
last_part_range VARCHAR := $2;
current_part_range VARCHAR := 'd';
--last_part_min_value TIMESTAMP := $1;
lastpart_min_value TIMESTAMP := $1;
--lastpart_min_value TIMESTAMP = '2015-07-28'; --(middle of the week)
--lastpart_min_value TIMESTAMP = '2015-12-28'; --(monday of the week)
--lastpart_min_value TIMESTAMP = '2016-01-03'; --(sunday of the week)
--lastpart_min_value TIMESTAMP = '2015-12-31'; 
--lastpart_min_value TIMESTAMP = '2016-01-01'; 
lastpart_max_value TIMESTAMP;
--current_min_value TIMESTAMP;
--current_max_value TIMESTAMP;
current_min_value_text VARCHAR='';
current_max_value_text VARCHAR='';

BEGIN
	--Postgres magic: 13 converts to next year!
	--current_min_value_text = '2017' || '-' || '15' || '-' || '01 00:00';
	--current_min_value = to_timestamp(current_min_value_text,'YYYY-MM-DD HH24:MI');
	--RAISE NOTICE 'current min value test 13 month: %',current_min_value;
     CASE last_part_range 
            WHEN 'd' THEN
                RAISE NOTICE '##############last: DAY';
                current_min_value = lastpart_min_value + INTERVAL '1 day';
            WHEN 'w' THEN
                RAISE NOTICE '##############last: WEEK, where day of the week is %', EXTRACT(ISODOW FROM lastpart_min_value);
                current_min_value = lastpart_min_value + INTERVAL '1 day' + INTERVAL '1 day' * (7 - EXTRACT(ISODOW FROM lastpart_min_value));
            WHEN 'm' THEN
                RAISE NOTICE '##############last: MONTH';
                current_min_value_text = extract(YEAR from lastpart_min_value) || '-' || extract(Month from lastpart_min_value)+1 || '-' || '01 00:00';
                current_min_value = to_timestamp(current_min_value_text,'YYYY-MM-DD HH24:MI');
            ELSE
                RAISE NOTICE 'Unknown last partition range';
     END CASE;
     lastpart_max_value = current_min_value - INTERVAL '1 second';
     
     CASE current_part_range 
            WHEN 'd' THEN
                current_max_value = current_min_value + INTERVAL '1 day' - INTERVAL '1 second';
                RAISE NOTICE 'lastpart_min_value % , Curr_min_value %, Curr_max_value %',lastpart_min_value,current_min_value,current_max_value;
            WHEN 'w' THEN
                current_max_value = current_min_value + INTERVAL '1 day' + INTERVAL '1 day' * (7 - EXTRACT(ISODOW FROM current_min_value)) - INTERVAL '1 SECOND';
                RAISE NOTICE 'lastpart_min_value % , Curr_min_value %, Curr_max_value %',lastpart_min_value,current_min_value,current_max_value;
            WHEN 'm' THEN
                current_max_value_text = extract(YEAR from current_min_value) || '-' || extract(Month from current_min_value)+1 || '-' || '01 00:00';
                current_max_value = to_timestamp(current_max_value_text,'YYYY-MM-DD HH24:MI') - INTERVAL '1 second';
                RAISE NOTICE 'lastpart_min_value % , Curr_min_value %, Curr_max_value %',lastpart_min_value,current_min_value,current_max_value;
            ELSE
                RAISE NOTICE 'Unknown current partition range';
     END CASE;

END
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
 
ALTER FUNCTION find_max (in TIMESTAMP,in VARCHAR,out current_min_value TIMESTAMP, out current_max_value TIMESTAMP)
OWNER TO zabbix;

--test
SELECT find_max('2015-07-28','d');



CREATE OR REPLACE FUNCTION partition_maintenance(VARCHAR, VARCHAR, VARCHAR, INTEGER DEFAULT NULL, INTEGER DEFAULT 1, BOOLEAN DEFAULT FALSE)
RETURNS void AS
$BODY$
DECLARE
  master_table    VARCHAR := $1;
  partition_range VARCHAR := $2;
  object_owner    VARCHAR := $3;
  number_to_keep  INTEGER := $4;
  number_to_drop  INTEGER := $5;
  dry_run         BOOLEAN := $6;
  range_interval  INTEGER := EXTRACT(EPOCH FROM partition_range::INTERVAL);
  now             INTEGER := EXTRACT(EPOCH FROM NOW())::INT;
 
  current_range     INTEGER := now / range_interval * range_interval;  
  current_partition VARCHAR := FORMAT('%s_p%s', master_table, current_range);
  current_check_min INTEGER := current_range;
  current_check_max INTEGER := current_check_min + range_interval;
 
  previous_range     INTEGER := current_range - range_interval;
  previous_partition VARCHAR := FORMAT('%s_p%s', master_table, previous_range);
  previous_check_min INTEGER := previous_range;
  previous_check_max INTEGER := previous_check_min + range_interval;
 
  next_range     INTEGER := current_range + range_interval;
  next_partition VARCHAR := FORMAT('%s_p%s', master_table, next_range);
  next_check_min INTEGER := next_range;
  next_check_max INTEGER := next_check_min + range_interval;
 
  function_name VARCHAR := FORMAT('%s_part_trig_func()', master_table);
 
  create_function VARCHAR := FORMAT(
    'CREATE OR REPLACE FUNCTION %s
         RETURNS trigger AS
       $$ 
       BEGIN
         IF TG_OP = ''INSERT'' THEN 
           IF NEW.clock >= %s AND NEW.clock < %s THEN 
             INSERT INTO %I VALUES (NEW.*); 
           ELSIF NEW.clock >= %s AND NEW.clock < %s THEN 
             INSERT INTO %I VALUES (NEW.*);
           ELSIF NEW.clock >= %s AND NEW.clock < %s THEN 
             INSERT INTO %I VALUES (NEW.*);
           ELSE
             RETURN NEW;
           END IF;
         END IF; 
         RETURN NULL; 
       END $$
       LANGUAGE plpgsql VOLATILE
       COST 100',
  function_name,
  current_check_min,
  current_check_max,
  current_partition,
  next_check_min,
  next_check_max,
  next_partition,
  previous_check_min,
  previous_check_max,
  previous_partition);
 
  alter_function VARCHAR := FORMAT('ALTER FUNCTION %s OWNER TO %s;', function_name, object_owner);
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_user WHERE usename = object_owner) THEN
    RAISE EXCEPTION 'User ''%'' does not exist.', object_owner;
  ELSIF NOT EXISTS (SELECT 1 FROM pg_tables WHERE tablename = master_table) THEN
    RAISE EXCEPTION 'Master table ''%'' does not exist.', master_table;
  ELSE
    PERFORM add_partition(master_table, previous_partition, previous_check_min, previous_check_max, object_owner, dry_run);
    PERFORM add_partition(master_table, current_partition,  current_check_min,  current_check_max,  object_owner, dry_run);
    PERFORM add_partition(master_table, next_partition,     next_check_min,     next_check_max,     object_owner, dry_run);
 
    IF dry_run = FALSE THEN
      EXECUTE create_function;
      EXECUTE alter_function;
    ELSE
      RAISE INFO '%', create_function;
      RAISE INFO '%', alter_function;
    END IF;
 
    IF number_to_keep IS NOT NULL THEN
      PERFORM remove_partitions(master_table, current_range, range_interval, number_to_keep, number_to_drop, dry_run);
    END IF;
  END IF;
END
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
 
ALTER FUNCTION partition_maintenance(VARCHAR, VARCHAR, VARCHAR, INTEGER, INTEGER, BOOLEAN)
OWNER TO zabbix;
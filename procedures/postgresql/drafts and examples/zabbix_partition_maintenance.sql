CREATE OR REPLACE FUNCTION zabbix_partition_maintenance(VARCHAR, INTEGER DEFAULT NULL, INTEGER DEFAULT NULL, INTEGER DEFAULT 0, VARCHAR DEFAULT 'zabbix', BOOLEAN DEFAULT FALSE)
RETURNS void AS
$BODY$
DECLARE
  partition_range        VARCHAR := $1;
  history_number_to_keep INTEGER := $2;
  trends_number_to_keep  INTEGER := $3;
  number_to_drop         INTEGER := $4;
  object_owner           VARCHAR := $5;
  dry_run                BOOLEAN := $6;
BEGIN
  PERFORM partition_maintenance('history',      partition_range, object_owner, history_number_to_keep, number_to_drop, dry_run);
  PERFORM partition_maintenance('history_log',  partition_range, object_owner, history_number_to_keep, number_to_drop, dry_run);
  PERFORM partition_maintenance('history_str',  partition_range, object_owner, history_number_to_keep, number_to_drop, dry_run);
  PERFORM partition_maintenance('history_text', partition_range, object_owner, history_number_to_keep, number_to_drop, dry_run);
  PERFORM partition_maintenance('history_uint', partition_range, object_owner, history_number_to_keep, number_to_drop, dry_run);
 
  PERFORM partition_maintenance('trends',      partition_range, object_owner, trends_number_to_keep, number_to_drop, dry_run);
  PERFORM partition_maintenance('trends_uint', partition_range, object_owner, trends_number_to_keep, number_to_drop, dry_run);
END
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
 
ALTER FUNCTION zabbix_partition_maintenance(VARCHAR, INTEGER, INTEGER, INTEGER, VARCHAR, BOOLEAN)
OWNER TO zabbix;
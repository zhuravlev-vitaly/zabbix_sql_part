CREATE OR REPLACE FUNCTION remove_partitions(VARCHAR, INTEGER, INTEGER, INTEGER, INTEGER, BOOLEAN DEFAULT FALSE)
RETURNS void AS
$BODY$
DECLARE
  master_table   VARCHAR := $1;
  current_range  INTEGER := $2;
  range_interval INTEGER := $3;
  number_to_keep INTEGER := $4;
  number_to_drop INTEGER := $5;
  dry_run        BOOLEAN := $6;
  max_range      INTEGER := current_range - range_interval * number_to_keep;
  min_range      INTEGER := max_range - range_interval * (number_to_drop - 1);
  child_table    VARCHAR;
  drop_table     VARCHAR;
BEGIN
  FOR range_to_drop IN REVERSE max_range .. min_range BY range_interval LOOP
    child_table := FORMAT('%s_p%s', master_table, range_to_drop);
 
    IF EXISTS (SELECT 1 FROM pg_tables WHERE tablename = child_table) THEN
      drop_table := FORMAT('DROP TABLE %I', child_table);
 
      IF dry_run = FALSE THEN
        EXECUTE drop_table;
      ELSE
        RAISE INFO '%', drop_table;
      END IF;
    ELSE
      RAISE INFO 'Child table ''%'' does not exist. Stopping.', child_table;
      EXIT;
    END IF;
  END LOOP;
END
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
 
ALTER FUNCTION remove_partitions(VARCHAR, INTEGER, INTEGER, INTEGER, INTEGER, BOOLEAN)
OWNER TO zabbix;
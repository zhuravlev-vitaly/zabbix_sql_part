-- Zabbix official partitioning solution prototype
-- Version 0.1
-- Currently not for the production
CREATE OR REPLACE FUNCTION add_partition(VARCHAR, VARCHAR, INTEGER, INTEGER, VARCHAR, BOOLEAN DEFAULT FALSE)
RETURNS void AS
$BODY$
DECLARE
  master_table VARCHAR := $1;
  child_table  VARCHAR := $2;
  check_min    INTEGER := $3;
  check_max    INTEGER := $4;
  table_owner  VARCHAR := $5;
  dry_run      BOOLEAN := $6;
 
  create_table VARCHAR := FORMAT(
    'CREATE TABLE %I (
         CHECK (clock >= %s AND clock < %s),
         LIKE %I
         INCLUDING DEFAULTS INCLUDING CONSTRAINTS INCLUDING INDEXES INCLUDING STORAGE INCLUDING COMMENTS)',
    child_table,
    check_min,
    check_max,
    master_table);
 
  alter_table_owner   VARCHAR := FORMAT('ALTER TABLE %I OWNER TO %I', child_table, table_owner);
  alter_table_inherit VARCHAR := FORMAT('ALTER TABLE %I INHERIT %I', child_table, master_table);
BEGIN
  IF EXISTS (SELECT 1 FROM pg_tables WHERE tablename = child_table ) THEN
    RAISE NOTICE 'Child table ''%'' already exists. Skipping.', child_table;
  ELSE
    IF dry_run = FALSE THEN
      EXECUTE create_table;
      EXECUTE alter_table_owner;
      EXECUTE alter_table_inherit;
    ELSE
      RAISE INFO '%', create_table;
      RAISE INFO '%', alter_table_owner;
      RAISE INFO '%', alter_table_inherit;
    END IF;
  END IF;
END
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
 
ALTER FUNCTION add_partition(VARCHAR, VARCHAR, INTEGER, INTEGER, VARCHAR, BOOLEAN)
OWNER TO zabbix;
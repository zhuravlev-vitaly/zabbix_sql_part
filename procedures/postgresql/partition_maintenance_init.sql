-- Zabbix official partitioning solution prototype
-- Version 0.1
-- Currently not for the production
  CREATE OR REPLACE FUNCTION partition_maintenance_init (VARCHAR)
RETURNS void AS
$BODY$
DECLARE
object_owner VARCHAR := $1;
table_record RECORD;
  
BEGIN
    FOR table_record IN SELECT * FROM manage_partitions
    LOOP
        RAISE NOTICE '###############################################';
        RAISE NOTICE 'Run partition_maintenance for % where period %  days_to_keep % and days_in_advance %',
            table_record.tablename,table_record.period,table_record.days_to_keep,table_record.days_in_advance;
     
        PERFORM partition_maintenance(table_record.tablename,table_record.period,table_record.days_to_keep,table_record.days_in_advance,object_owner);
	END LOOP;
END
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
 
ALTER FUNCTION partition_maintenance_init (VARCHAR) OWNER TO zabbix;

--SELECT partition_maintenance_init('zabbix');
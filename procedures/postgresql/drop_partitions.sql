-- Zabbix official partitioning solution prototype
-- Version 0.1
-- Currently not for the production
CREATE OR REPLACE FUNCTION drop_partitions(VARCHAR, partition ARRAY,INTEGER, BOOLEAN DEFAULT FALSE, OUT part_array_out partition ARRAY)
AS
$BODY$
DECLARE
  master_table   VARCHAR := $1;
  part_array partition ARRAY := $2;
  days_to_keep   INTEGER := $3;
  dry_run        BOOLEAN := $4;
  delete_older_than TIMESTAMP;
  part           RECORD;
  current_p_min_value TIMESTAMP;
  current_p_max_value TIMESTAMP;
  current_p_name    VARCHAR;
  drop_table     VARCHAR;
  index_of_last_partition_not_dropped INT := array_length(part_array, 1);

BEGIN
        delete_older_than = NOW() - INTERVAL '1 day' * days_to_keep;
        FOR i IN REVERSE array_length(part_array, 1)..1 LOOP
            current_p_min_value = part_array[i].min_value;
            current_p_max_value = get_current_p_max_value(current_p_min_value,part_array[i].range);
            IF current_p_max_value < delete_older_than THEN -- delete until partition are old
                        current_p_name = part_array[i].name;
                        RAISE NOTICE 'Dropping partition %',current_p_name;
                        drop_table := FORMAT('DROP TABLE %I', current_p_name);
        
                  IF dry_run = FALSE THEN
                            EXECUTE drop_table;
                  ELSE
                            RAISE INFO '%', drop_table;
                  END IF;
            ELSE
                RAISE DEBUG 'Other partitions are more recent and should be kept';
                index_of_last_partition_not_dropped = i;
                EXIT;
            END IF;
        END LOOP;
        -- part_array_out: list of partitions that are not dropped. This will be returned
        part_array_out = part_array[1:index_of_last_partition_not_dropped];
END
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
 
ALTER FUNCTION drop_partitions(VARCHAR, partition ARRAY,INTEGER, BOOLEAN, OUT part_array_out partition ARRAY)
OWNER TO zabbix;
-- Zabbix official partitioning solution prototype
-- Version 0.1
-- Currently not for the production
 CREATE OR REPLACE FUNCTION get_current_p_max_value(in last_p_min_value TIMESTAMP,in last_p_range VARCHAR, out current_p_max_value TIMESTAMP)
AS
$BODY$
DECLARE
current_p_max_value_text VARCHAR;
BEGIN
	 CASE last_p_range 
            WHEN 'day','d','daily' THEN
                RAISE DEBUG '##############last: DAY';
                current_p_max_value = last_p_min_value + INTERVAL '1 day';
            WHEN 'week','w','weekly' THEN
                RAISE DEBUG '##############last: WEEK, where day of the week is %', EXTRACT(ISODOW FROM last_p_min_value);
                current_p_max_value = last_p_min_value + INTERVAL '1 day' + INTERVAL '1 day' * (7 - EXTRACT(ISODOW FROM last_p_min_value));
            WHEN 'month','m','monthly' THEN
                RAISE DEBUG '##############last: MONTH';
                current_p_max_value_text = extract(YEAR from last_p_min_value) || '-' || extract(Month from last_p_min_value)+1 || '-' || '01 00:00';
                current_p_max_value = to_timestamp(current_p_max_value_text,'YYYY-MM-DD HH24:MI');
            ELSE
                RAISE EXCEPTION 'Unknown current partition range';
     END CASE;     
END
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
 
ALTER FUNCTION get_current_p_max_value(in last_p_min_value TIMESTAMP,in last_p_range VARCHAR, out current_p_max_value TIMESTAMP)
OWNER TO zabbix;

--test
SELECT get_current_p_max_value('2015-07-28','d');



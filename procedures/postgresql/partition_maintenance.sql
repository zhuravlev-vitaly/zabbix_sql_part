-- Zabbix official partitioning solution prototype
-- Version 0.1
-- Currently not for the production
  CREATE OR REPLACE FUNCTION partition_maintenance (VARCHAR,VARCHAR, INT,INT, VARCHAR)
RETURNS void AS
$BODY$
DECLARE
  master_table    VARCHAR := $1;
  partition_range VARCHAR := substring($2 from 1 for 1);--grab first letter if for example 'week' is provided
  days_to_keep INT := $3;
  days_in_advance INT := $4;
  object_owner VARCHAR := $5;
  -- to check master_table existence of records
  mtable_record RECORD;
  mtable_has_partitions BOOLEAN;
  mtable_has_records    BOOLEAN;
  
  current_p_name VARCHAR;
  current_p_min_value TIMESTAMP;
  current_p_max_value TIMESTAMP; --current_p_max_value is max value but not included itself (less than constraint)
  current_p_min_value_epoch INT;
  current_p_max_value_epoch INT;
  
  add_partitions_until TIMESTAMP;

  part RECORD;
  part_array partition ARRAY;
  part_array_element partition;

  --trigger vars
  current_if_word VARCHAR := 'IF';
  function_name VARCHAR := FORMAT('%s_part_trig_func()', master_table);
  function_start VARCHAR := FORMAT(
    'CREATE OR REPLACE FUNCTION %s
         RETURNS trigger AS
       $$ 
       BEGIN
         IF TG_OP = ''INSERT'' THEN 
           ',
  function_name);
  function_end VARCHAR := FORMAT(
    '      ELSE
            RETURN NEW;
           END IF;
         END IF; 
         RETURN NULL; 
       END $$
       LANGUAGE plpgsql VOLATILE
       COST 100',
  function_name);
  function_middle VARCHAR := '';
  create_function VARCHAR;
  alter_function VARCHAR := FORMAT('ALTER FUNCTION %s OWNER TO %s;', function_name, object_owner);
  
  i INTEGER := 0;
  
BEGIN
    --check master_table for records and retrieve min clock value 
    EXECUTE 'SELECT MIN(clock) as min_clock FROM ONLY ' || quote_ident(master_table) INTO mtable_record;
    IF  mtable_record.min_clock IS NOT NULL THEN 
        mtable_has_records = TRUE;
    ELSE
        mtable_has_records = FALSE;
    END IF;
    
        --grab all child tables(partitions if available)
        FOR part IN SELECT  
                c.relname AS p_name,
				substring(c.relname from 'p_(\d+-\d+-\d+)_[dmw]$') AS p_timestamp,
                substring(c.relname from 'p_\d+-\d+-\d+_([dmw])$') AS p_range
			FROM pg_inherits 
			JOIN pg_class AS c ON (inhrelid=c.oid AND c.relname ~ '.+p_\d+-\d+-\d+_[dmw]$') 
			JOIN pg_class as p ON (inhparent=p.oid AND p.relname = master_table)
			ORDER BY  p_timestamp DESC
        LOOP -- place results into array part_array
            part_array[i+1] = part;
            RAISE DEBUG '% found PARTITION %' ,master_table,part_array[i+1];
            i = i +1;
        END LOOP;
    IF FOUND THEN
        mtable_has_partitions = TRUE;
    ELSE 
        mtable_has_partitions = FALSE;
    END IF;
        
    
    IF  mtable_has_records THEN 
        IF mtable_has_partitions THEN
            RAISE NOTICE 'Has data in mastertable but partitions already exist. It is unsafe to create new partitions for old data now.';
            current_p_min_value = get_current_p_max_value(part_array[1].min_value,part_array[1].range);
        ELSE
            RAISE NOTICE 'min_clock in MASTER_TABLE is: %' ,mtable_record.min_clock;
            RAISE NOTICE 'min_clock in MASTER_TABLE is (in timestamp): %' ,to_timestamp(mtable_record.min_clock);
            -- last part max is minclock (careful with 1 sec interval)
            current_p_min_value = date_trunc('day', to_timestamp(mtable_record.min_clock));
        END IF;
            
    ELSIF mtable_has_records = FALSE  THEN --no records in master_table
        RAISE NOTICE 'No records found inside parent TABLE %',master_table;
            IF mtable_has_partitions THEN --already partitioned
                RAISE NOTICE 'TABLE % already has partitions',master_table;
                current_p_min_value = get_current_p_max_value(part_array[1].min_value,part_array[1].range);
            ELSE -- no partitions exist
                RAISE NOTICE 'Completely pure table %: no partitions or records',master_table;
                RAISE DEBUG 'current date is %',now()::DATE;
                current_p_min_value = now()::DATE;
            END IF;
    ELSE
        RAISE EXCEPTION 'mtable_has_records variable is not initialized';
    END IF;
    RAISE NOTICE 'Current partition min_value is %', current_p_min_value;
    --RAISE NOTICE 'Current partition max_value is %', current_p_max_value;
    --#####################################ADD NEW PARTITIONS PART
    add_partitions_until = NOW() + INTERVAL '1 day' * days_in_advance;
    RAISE NOTICE 'add_partitions_until variable is % and current_p_min_value is %',add_partitions_until,current_p_min_value;

    WHILE current_p_min_value < add_partitions_until LOOP
            current_p_max_value = get_current_p_max_value(current_p_min_value,partition_range);
            
            current_p_min_value_epoch = EXTRACT(EPOCH FROM current_p_min_value);
            current_p_max_value_epoch = EXTRACT(EPOCH FROM current_p_max_value);
            current_p_name = FORMAT('%s_p_%s_%s', master_table,current_p_min_value::DATE,partition_range);
            RAISE NOTICE 'Creating partition % .... from %  less than %',current_p_name,current_p_min_value,current_p_max_value;
            PERFORM add_partition(master_table, current_p_name, current_p_min_value_epoch, current_p_max_value_epoch,object_owner,FALSE);
            
            --prepend for trigger update:
             part_array = array_prepend(ROW(current_p_name,current_p_min_value,partition_range)::partition , part_array); -- pop new element to array
            
            -- prepare for the next round:
            current_p_min_value = current_p_max_value;
          
    END LOOP;
	
    --remove old partitions
    RAISE DEBUG 'Before removal %', part_array;
    part_array = drop_partitions(master_table,part_array,days_to_keep,FALSE);
    RAISE DEBUG 'After removal %',  part_array;
    
    
    --prepare trigger
    i = 1;
	FOREACH part_array_element IN ARRAY part_array LOOP

        IF i > 1 THEN
            current_if_word = 'ELSIF';
        END IF;
        current_p_min_value = part_array[i].min_value;
        current_p_max_value = get_current_p_max_value(current_p_min_value,part_array[i].range);
        current_p_name = FORMAT('%s_p_%s_%s', master_table, current_p_min_value::DATE,part_array[i].range );
        --prepare for insert into trigger
        current_p_min_value_epoch = EXTRACT(EPOCH FROM current_p_min_value);
        current_p_max_value_epoch = EXTRACT(EPOCH FROM current_p_max_value);
		function_middle = function_middle || FORMAT('%s NEW.clock >= %s AND NEW.clock < %s THEN 
            INSERT INTO %I VALUES (NEW.*);
        ',current_if_word,current_p_min_value_epoch,current_p_max_value_epoch,current_p_name);
		i = i + 1;
	END LOOP;
	
      RAISE DEBUG 'function code:
       %
       %
       %', function_start,function_middle,function_end;
      create_function = function_start || function_middle || function_end;
      EXECUTE create_function;
      EXECUTE alter_function;
    
    -- move data from mtable to partitions if necessary
    IF mtable_has_records THEN
            RAISE NOTICE 'Start moving data from master table to partitions....';
            EXECUTE 'INSERT INTO ' || quote_ident(master_table) || ' SELECT * FROM ONLY ' || quote_ident(master_table);
            EXECUTE 'TRUNCATE ONLY ' || quote_ident(master_table);
    END IF;
END
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
 
ALTER FUNCTION partition_maintenance (VARCHAR,VARCHAR, INT,INT, VARCHAR) OWNER TO zabbix;

--test
--SELECT partition_maintenance('history_text','week',120,30,'zabbix');
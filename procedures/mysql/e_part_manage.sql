-- Zabbix official partitioning solution prototype
-- Version 0.1
-- Currently not for the production
DELIMITER $$
 
USE `zabbix`$$
 
CREATE EVENT IF NOT EXISTS `e_part_manage`
       ON SCHEDULE EVERY 1 DAY
       STARTS '2011-08-08 04:00:00'
       ON COMPLETION PRESERVE
       ENABLE
       COMMENT 'Adding and dropping partitions'
       DO BEGIN
            CALL partition_maintenance_init('zabbix');
            CALL drop_partitions('zabbix');
       END$$
DELIMITER ;

-- Zabbix official partitioning solution prototype
-- Version 0.1
-- Currently not for the production
CREATE TABLE `manage_partitions` (
  `tablename` VARCHAR(64) NOT NULL COMMENT 'Table name',
  `period` VARCHAR(64) NOT NULL COMMENT 'Period - daily,monthly,weekly',
  `days_to_keep` INT(3) UNSIGNED NOT NULL DEFAULT '120' COMMENT 'For how many days to keep the partitions',
  `days_in_advance` INT(3) UNSIGNED NOT NULL DEFAULT '7' COMMENT 'How many partitions create in advance',
  `last_updated` DATETIME DEFAULT NULL COMMENT 'When a partition was added last time',
  `comments` VARCHAR(128) DEFAULT '1' COMMENT 'Comments',
  PRIMARY KEY (`tablename`)
) ENGINE=INNODB;
INSERT INTO manage_partitions (tablename, period, days_to_keep,days_in_advance, last_updated, comments) VALUES ('history', 'day', 182,7, now(), '');
INSERT INTO manage_partitions (tablename, period, days_to_keep,days_in_advance, last_updated, comments) VALUES ('history_uint', 'day', 182,7, now(), '');
INSERT INTO manage_partitions (tablename, period, days_to_keep,days_in_advance, last_updated, comments) VALUES ('history_str', 'day', 182,7, now(), '');
INSERT INTO manage_partitions (tablename, period, days_to_keep,days_in_advance, last_updated, comments) VALUES ('history_text', 'day', 182,7, now(), '');
INSERT INTO manage_partitions (tablename, period, days_to_keep,days_in_advance, last_updated, comments) VALUES ('history_log', 'day', 182,7, now(), '');
INSERT INTO manage_partitions (tablename, period, days_to_keep,days_in_advance, last_updated, comments) VALUES ('trends', 'month',730,30, now(), '');
INSERT INTO manage_partitions (tablename, period, days_to_keep,days_in_advance, last_updated, comments) VALUES ('trends_uint', 'month', 730,30, now(), '');

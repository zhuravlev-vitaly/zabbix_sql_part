-- Zabbix official partitioning solution prototype
-- Version 0.1
-- Currently not for the production
DELIMITER $$

USE `zabbix`$$
 
DROP PROCEDURE IF EXISTS `get_current_p_max_value`$$
 
CREATE PROCEDURE `get_current_p_max_value`(last_p_min_value TIMESTAMP,last_p_range varchar(1), OUT current_p_max_value TIMESTAMP)
BEGIN
   -- SELECT CONCAT(last_p_min_value,' ',last_p_range) AS DEBUG;
    
   SET last_p_min_value = DATE(last_p_min_value);
    CASE last_p_range 
        WHEN 'd' THEN
            SET current_p_max_value = last_p_min_value + INTERVAL 1 DAY;
        WHEN 'w' THEN
            SET current_p_max_value = last_p_min_value + INTERVAL 1 DAY + INTERVAL (6 - WEEKDAY(last_p_min_value)) DAY;
        WHEN 'm' THEN
            SET current_p_max_value = last_p_min_value - INTERVAL (DAY(last_p_min_value)) DAY + INTERVAL 1 DAY + INTERVAL 1 MONTH;
        ELSE
            SELECT 'get_current_p_max_value() has no proper return value';
    END CASE;   

    -- SELECT CONCAT('min is ',last_p_min_value,' and max is ',current_p_max_value) AS DEBUG;
END$$

DELIMITER ;
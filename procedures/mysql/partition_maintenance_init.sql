-- Zabbix official partitioning solution prototype
-- Version 0.1
-- Currently not for the production
DELIMITER $$

USE `zabbix`$$
DROP PROCEDURE IF EXISTS `partition_maintenance_init`$$
 
CREATE PROCEDURE `partition_maintenance_init`(IN_SCHEMANAME VARCHAR(64))
BEGIN
    DECLARE TABLENAME_TMP VARCHAR(64);
    DECLARE current_partition_range VARCHAR(1);
    DECLARE current_days_in_advance INT;
    DECLARE DONE INT DEFAULT 0;
 
    DECLARE get_prt_tables CURSOR FOR
        SELECT `tablename`, SUBSTR(`period`,1,1) AS partition_range,`days_in_advance`
            FROM manage_partitions;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
 
    OPEN get_prt_tables;
 
    loop_create_part: LOOP
        IF DONE THEN
            LEAVE loop_create_part;
        END IF;
 
        FETCH get_prt_tables INTO TABLENAME_TMP, current_partition_range,current_days_in_advance;
        CALL `add_partitions`(IN_SCHEMANAME, TABLENAME_TMP,current_days_in_advance,current_partition_range);
        UPDATE manage_partitions SET last_updated = NOW() WHERE tablename = TABLENAME_TMP;

    END LOOP loop_create_part;
 
    CLOSE get_prt_tables;
END$$

DELIMITER ;
-- Zabbix official partitioning solution prototype
-- Version 0.1
-- Currently not for the production
DELIMITER $$

USE `zabbix`$$
 
DROP PROCEDURE IF EXISTS `drop_partitions`$$
 
CREATE PROCEDURE `drop_partitions`(IN_SCHEMANAME VARCHAR(64))
BEGIN
    DECLARE DONE INT DEFAULT 0;
    DECLARE ERROR INT DEFAULT 0;
    DECLARE current_table_name VARCHAR(64);
    DECLARE current_p_name VARCHAR(128);
    DECLARE current_p_max_value TIMESTAMP;
    DECLARE days_to_keep INT;
    DECLARE get_partitions CURSOR FOR
        SELECT p.`table_name`, p.`partition_name`, FROM_UNIXTIME( LTRIM( RTRIM(p.`partition_description`) ) ) AS partition_max, mp.`days_to_keep`
            FROM information_schema.partitions p
            JOIN manage_partitions mp ON mp.tablename = p.TABLE_NAME
            WHERE p.table_schema = IN_SCHEMANAME AND p.`partition_name` IS NOT NULL
            ORDER BY p.TABLE_NAME, partition_max;
 
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    DECLARE CONTINUE HANDLER FOR 1508
      BEGIN
        SELECT CONCAT('Cannot remove all partitions, use DROP TABLE instead for ',current_p_name,' and table ',current_table_name) AS RESULT;
        SET ERROR=1;
    END;
    
    OPEN get_partitions;
 
    loop_check_prt: LOOP
        IF DONE THEN
            LEAVE loop_check_prt;
        END IF;
        
        FETCH get_partitions INTO current_table_name,current_p_name,current_p_max_value,days_to_keep;
        
        SET @delete_older_than = NOW() - INTERVAL days_to_keep DAY;
        -- SELECT CONCAT('@delete_older_than: ',@delete_older_than,' for table ',current_table_name) AS DEBUG;
        
        IF current_p_max_value < @delete_older_than THEN
            CALL drop_old_partition(IN_SCHEMANAME, current_table_name, current_p_name);
        END IF;
    END LOOP loop_check_prt;
 
    CLOSE get_partitions;
END$$
 
DELIMITER ;
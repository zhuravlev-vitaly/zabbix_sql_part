-- Zabbix official partitioning solution prototype
-- Version 0.1
-- Currently not for the production
DELIMITER $$
 
USE `zabbix`$$
 
DROP PROCEDURE IF EXISTS `add_partitions`$$
 
CREATE PROCEDURE `add_partitions`(IN_SCHEMANAME VARCHAR(64), IN_TABLENAME VARCHAR(64),days_in_advance INT,partition_range VARCHAR(1))
BEGIN
    DECLARE ERROR INT UNSIGNED;
    DECLARE ROWS_CNT INT UNSIGNED;
    DECLARE last_PARTITIONNAME VARCHAR(16);
    DECLARE last_PARTITIONMAX TIMESTAMP;
    DECLARE table_has_partitions BOOLEAN;
    DECLARE table_has_records BOOLEAN;
    DECLARE DONE INT DEFAULT 0;
    DECLARE get_partitions CURSOR FOR
        SELECT partition_name,FROM_UNIXTIME(LTRIM(RTRIM(PARTITION_DESCRIPTION))) AS partition_max
                FROM information_schema.partitions
                WHERE table_schema = IN_SCHEMANAME AND TABLE_NAME = IN_TABLENAME AND partition_name IS NOT NULL
                ORDER BY partition_max DESC; -- latest first
    DECLARE CONTINUE HANDLER FOR 1503
      BEGIN
        SELECT CONCAT('A PRIMARY KEY must include all columns in the table s partitioning function: failed creating ',@current_p_name,'...') AS RESULT;
        SET ERROR=1;
    END;
    DECLARE CONTINUE HANDLER FOR 1493
      BEGIN
        SELECT CONCAT('VALUES LESS THAN value must be strictly increasing, you have more recent partitions, skipping creation of ',@current_p_name,'...') AS RESULT;
        SET ERROR=1;
    END;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE = 1;

    SET ERROR = 0;
    
    SET @add_partitions_until = NULL;
    SET @add_partitions_until = NOW() + INTERVAL days_in_advance DAY;
    SELECT CONCAT('@add_partitions_until: ',@add_partitions_until,' for table: ',IN_TABLENAME) AS DEBUG;
    
    -- check whether partitions exist
    SET @ROWS_CNT := 0;
    SELECT COUNT(*) INTO @ROWS_CNT
            FROM information_schema.partitions
            WHERE table_schema = IN_SCHEMANAME AND TABLE_NAME = IN_TABLENAME AND PARTITION_NAME IS NOT NULL;

    IF @ROWS_CNT > 0 THEN
        SELECT concat('@ROWS_CNT is ', @ROWS_CNT, ' so table ',IN_TABLENAME,' has partitions') AS DEBUG;       
        SET table_has_partitions := TRUE;
    -- ELSEIF @ROWS_CNT = 0 THEN
    -- SELECT concat('@ROWS_CNT is ', @ROWS_CNT, ' so table ',IN_TABLENAME,' has no partitions') AS DEBUG;       
    -- SET table_has_partitions := FALSE;
    ELSE
        SELECT concat('@ROWS_CNT is NULL, so table ',IN_TABLENAME,' has no partitions') AS DEBUG;       
        SET table_has_partitions := FALSE;
    END IF;
    
    
    
    IF table_has_partitions = FALSE THEN -- if there are no partitions then check whether table has records 
        SET @MIN_CLOCK :=0;
        SET @sql = CONCAT('SELECT MIN(clock) INTO @MIN_CLOCK  FROM ',IN_TABLENAME);
        PREPARE STMT FROM @sql;
        EXECUTE STMT;
        DEALLOCATE PREPARE STMT;
        
        IF @MIN_CLOCK > 0 THEN
            SELECT concat('@MIN_CLOCK is ', @MIN_CLOCK, ' so table ',IN_TABLENAME,' has records') AS DEBUG;       
            SET table_has_records := TRUE;
            CALL partition_table_initial(IN_SCHEMANAME, IN_TABLENAME, @add_partitions_until,partition_range,@MIN_CLOCK);
--         ELSEIF @MIN_CLOCK IS NULL THEN
--             SELECT concat('Table has no records') AS DEBUG;
--             SET table_has_records := FALSE;
        ELSE
            SELECT concat('Table ',IN_TABLENAME,' has no records') AS DEBUG;
            SET table_has_records := FALSE;
            CALL partition_table_initial(IN_SCHEMANAME, IN_TABLENAME, @add_partitions_until,partition_range,UNIX_TIMESTAMP(NOW()) div 1);
        END IF;
    
    ELSE -- has partitions
        OPEN get_partitions;
                FETCH get_partitions INTO last_PARTITIONNAME,last_PARTITIONMAX;
                  SELECT CONCAT('Latest partition exists: ',last_PARTITIONNAME,' with values LESS THAN ',last_PARTITIONMAX) AS DEBUG;
                        
                        SET @current_p_min_value := last_PARTITIONMAX;
 
      
        loop_create_prt: WHILE @current_p_min_value < @add_partitions_until DO
            SET ERROR = 0;

            
            CALL get_current_p_max_value(@current_p_min_value,partition_range,@current_p_max_value);
            SET @current_p_max_value_epoch = UNIX_TIMESTAMP(@current_p_max_value) div 1;
            SET @current_p_name = CONCAT('p_',DATE_FORMAT( @current_p_min_value, '%Y_%m_%d' ),'_',partition_range);
            
            SELECT CONCAT ( 'Creating partition ',@current_p_name,' in table ',IN_TABLENAME,'.... from ',@current_p_min_value,'  less than ',@current_p_max_value) AS DEBUG;
                SET @SQL = CONCAT( 'ALTER TABLE `', IN_SCHEMANAME, '`.`', IN_TABLENAME, '`',
                    ' ADD PARTITION (PARTITION ', @current_p_name, ' VALUES LESS THAN (', @current_p_max_value_epoch, '));' );
     
            PREPARE STMT FROM @SQL;
            EXECUTE STMT;
            DEALLOCATE PREPARE STMT;
    
            IF ERROR = 0 THEN
                SELECT CONCAT("Partition `", @current_p_name, "` for table `",IN_SCHEMANAME, ".", IN_TABLENAME, "` has been added") AS RESULT;
            END IF;
            
            -- prepare for the next round:
            SET @current_p_min_value := @current_p_max_value;
            
        END WHILE;
                  
                
    END IF;

END$$
DELIMITER ;
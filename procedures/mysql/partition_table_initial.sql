-- Zabbix official partitioning solution prototype
-- Version 0.1
-- Currently not for the production
DELIMITER $$

USE `zabbix`$$
DROP PROCEDURE IF EXISTS `partition_table_initial`$$
 
CREATE PROCEDURE `partition_table_initial`(IN_SCHEMANAME VARCHAR(64), IN_TABLENAME VARCHAR(64), add_partitions_until TIMESTAMP,partition_range VARCHAR(1),MIN_CLOCK INT)
BEGIN
    DECLARE ERROR INT;    
    DECLARE CONTINUE HANDLER FOR 1503
      BEGIN
        SELECT CONCAT('A PRIMARY KEY must include all columns in the table s partitioning function: failed creating ',@current_p_name,'...') AS RESULT;
        SET ERROR=1;
    END;
    DECLARE CONTINUE HANDLER FOR 1091
      BEGIN
        SELECT CONCAT(' Cant DROP PRIMARY check that column/key exists for table ',IN_TABLENAME) AS RESULT;
        SET ERROR=1;
    END;
    SET @SQL_START := CONCAT('ALTER TABLE `', IN_SCHEMANAME, '`.`', IN_TABLENAME, '`',' PARTITION BY RANGE ( clock)',
    '(');
    SET @current_p_min_value := DATE(FROM_UNIXTIME( MIN_CLOCK ) );
    SET @SQL_MIDDLE := '';
    SET @SQL_END := ');';
    
    -- index autodrop for history_log,history_text
    IF IN_TABLENAME IN ('history_log','history_text') THEN
        SET @INDEX :=0;
        SET @INDEX_NAME := 'PRIMARY';
        SELECT COUNT(INDEX_NAME) INTO @INDEX
            FROM INFORMATION_SCHEMA.STATISTICS 
            WHERE `TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND `TABLE_NAME` = IN_TABLENAME
            AND `INDEX_NAME` = @INDEX_NAME;
        
        IF @INDEX > 0 THEN
            SELECT CONCAT('Trying to drop index PRIMARY for table ',IN_TABLENAME) AS DEBUG;
            SET ERROR :=0;
            SET @SQL_DROP_KEY := CONCAT('ALTER TABLE `', IN_SCHEMANAME, '`.`', IN_TABLENAME, '`',' DROP PRIMARY KEY , ADD INDEX `',IN_TABLENAME,'_0` (`id`);');
            PREPARE STMT FROM @SQL_DROP_KEY;
            EXECUTE STMT;
            DEALLOCATE PREPARE STMT;
        END IF;
        
        
        SET @INDEX :=0;
        SET @INDEX_NAME := CONCAT(IN_TABLENAME,'_2');
        SELECT COUNT(INDEX_NAME) INTO @INDEX
            FROM INFORMATION_SCHEMA.STATISTICS 
            WHERE `TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND `TABLE_NAME` = IN_TABLENAME
            AND `INDEX_NAME` = @INDEX_NAME;
        
        IF @INDEX > 0 THEN
            SELECT CONCAT('Trying to drop index ',@INDEX_NAME,' for table ',IN_TABLENAME) AS DEBUG;
            SET ERROR :=0;
            SET @SQL_DROP_KEY := CONCAT('ALTER TABLE `', IN_SCHEMANAME, '`.`', IN_TABLENAME, '`',' DROP KEY `',IN_TABLENAME,'_2`;');
            PREPARE STMT FROM @SQL_DROP_KEY;
            EXECUTE STMT;
            DEALLOCATE PREPARE STMT;
        END IF;
    END IF;
    
    loop_create_prt: WHILE @current_p_min_value < add_partitions_until DO

        CALL get_current_p_max_value(@current_p_min_value,partition_range,@current_p_max_value);
        SET @current_p_max_value_epoch = UNIX_TIMESTAMP(@current_p_max_value) div 1;
        SET @current_p_name = CONCAT('p_',DATE_FORMAT( @current_p_min_value, '%Y_%m_%d' ),'_',partition_range);
        
        -- SELECT CONCAT ( 'Creating partition ',@current_p_name,' .... from ',@current_p_min_value,'  less than ',@current_p_max_value);
        
        
        SET @SQL_MIDDLE = CONCAT( @SQL_MIDDLE,
                ' PARTITION ',@current_p_name,' VALUES LESS THAN (',@current_p_max_value_epoch,') ENGINE = InnoDB,');
        
        -- prepare for the next round:
        SET @current_p_min_value := @current_p_max_value;
            
    END WHILE;
    
    SET @SQL_MIDDLE := RPAD(@SQL_MIDDLE,LENGTH(@SQL_MIDDLE)-1,1); -- remove last ,
    SET @SQL := CONCAT(@SQL_START,@SQL_MIDDLE,@SQL_END);

    SET ERROR = 0;
    PREPARE STMT FROM @SQL;
    EXECUTE STMT;
    DEALLOCATE PREPARE STMT;
        
    IF ERROR = 0 THEN
        SELECT CONCAT("Table `",IN_SCHEMANAME, ".", IN_TABLENAME, "` has been partitioned for the first time") AS RESULT;
    END IF;
    
END$$

DELIMITER ;
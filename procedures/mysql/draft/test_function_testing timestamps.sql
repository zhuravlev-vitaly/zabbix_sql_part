  CREATE OR REPLACE FUNCTION my_test_date (VARCHAR,VARCHAR, INT)
RETURNS void AS
$BODY$
DECLARE
--lastpart_min_value TIMESTAMP = '2015-07-28'; --(middle of the week)
--lastpart_min_value TIMESTAMP = '2015-12-28'; --(monday of the week)
--lastpart_min_value TIMESTAMP = '2016-01-03'; --(sunday of the week)
--lastpart_min_value TIMESTAMP = '2015-12-31'; 
--lastpart_min_value TIMESTAMP = '2016-01-01'; 
current_min_value TIMESTAMP;
current_max_value TIMESTAMP;
current_min_value_text VARCHAR='';
current_max_value_text VARCHAR='';
  
BEGIN
	RAISE NOTICE '##############current: DAY - last: DAY';
	current_min_value = lastpart_min_value + INTERVAL '1 day';
	current_max_value = current_min_value + INTERVAL '1 day' - INTERVAL '1 second';
      	RAISE NOTICE 'lastpart_min_value % , Curr_min_value %, Curr_max_value %',lastpart_min_value,current_min_value,current_max_value;

      	RAISE NOTICE '##############current: DAY - last: WEEK, where day of the week is %', EXTRACT(ISODOW FROM lastpart_min_value);
	current_min_value = lastpart_min_value + INTERVAL '1 day' + INTERVAL '1 day' * (7 - EXTRACT(ISODOW FROM lastpart_min_value));
	current_max_value = current_min_value + INTERVAL '1 day' - INTERVAL '1 second';
      	RAISE NOTICE 'lastpart_min_value % , Curr_min_value %, Curr_max_value %',lastpart_min_value,current_min_value,current_max_value;

      	RAISE NOTICE '##############current: DAY - last: MONTH';
	current_min_value_text = extract(YEAR from lastpart_min_value) || '-' || extract(Month from lastpart_min_value)+1 || '-' || '01 00:00';
	current_min_value = to_timestamp(current_min_value_text,'YYYY-MM-DD HH24:MI');
	current_max_value = current_min_value + INTERVAL '1 day' - INTERVAL '1 second';
      	RAISE NOTICE 'lastpart_min_value % , Curr_min_value %, Curr_max_value %',lastpart_min_value,current_min_value,current_max_value;

	-- so current_max_value is actually the same for day;
      	

	RAISE NOTICE '##############current: WEEK - last: DAY';
	current_min_value = lastpart_min_value + INTERVAL '1 day';
	current_max_value = current_min_value + INTERVAL '1 day' + INTERVAL '1 day' * (7 - EXTRACT(ISODOW FROM current_min_value)) - INTERVAL '1 SECOND';
      	RAISE NOTICE 'lastpart_min_value % , Curr_min_value %, Curr_max_value %',lastpart_min_value,current_min_value,current_max_value;

      	RAISE NOTICE '##############current: WEEK - last: WEEK, where day of the week is %', EXTRACT(ISODOW FROM lastpart_min_value);
	current_min_value = lastpart_min_value + INTERVAL '1 day' + INTERVAL '1 day' * (7 - EXTRACT(ISODOW FROM lastpart_min_value));
	current_max_value = current_min_value + INTERVAL '1 day' + INTERVAL '1 day' * (7 - EXTRACT(ISODOW FROM current_min_value)) - INTERVAL '1 SECOND';
      	RAISE NOTICE 'lastpart_min_value % , Curr_min_value %, Curr_max_value %',lastpart_min_value,current_min_value,current_max_value;

      	RAISE NOTICE '##############current: WEEK - last: MONTH';
	current_min_value_text = extract(YEAR from lastpart_min_value) || '-' || extract(Month from lastpart_min_value)+1 || '-' || '01 00:00';
	current_min_value = to_timestamp(current_min_value_text,'YYYY-MM-DD HH24:MI');
	current_max_value = current_min_value + INTERVAL '1 day' + INTERVAL '1 day' * (7 - EXTRACT(ISODOW FROM current_min_value)) - INTERVAL '1 SECOND';
      	RAISE NOTICE 'lastpart_min_value % , Curr_min_value %, Curr_max_value %',lastpart_min_value,current_min_value,current_max_value;

        -- so current_max_value is actually the same for week;

        RAISE NOTICE '##############current: MONTH - last: DAY';
	current_min_value = lastpart_min_value + INTERVAL '1 day';
	current_max_value_text = extract(YEAR from current_min_value) || '-' || extract(Month from current_min_value)+1 || '-' || '01 00:00';
	current_max_value = to_timestamp(current_max_value_text,'YYYY-MM-DD HH24:MI') - interval '1 second';
      	RAISE NOTICE 'lastpart_min_value % , Curr_min_value %, Curr_max_value %',lastpart_min_value,current_min_value,current_max_value;

      	RAISE NOTICE '##############current: MONTH - last: WEEK, where day of the week is %', EXTRACT(ISODOW FROM lastpart_min_value);
	current_min_value = lastpart_min_value + INTERVAL '1 day' + INTERVAL '1 day' * (7 - EXTRACT(ISODOW FROM lastpart_min_value));
	current_max_value_text = extract(YEAR from current_min_value) || '-' || extract(Month from current_min_value)+1 || '-' || '01 00:00';
	current_max_value = to_timestamp(current_max_value_text,'YYYY-MM-DD HH24:MI') - interval '1 second';
      	
      	RAISE NOTICE 'lastpart_min_value % , Curr_min_value %, Curr_max_value %',lastpart_min_value,current_min_value,current_max_value;

      	RAISE NOTICE '##############current: MONTH - last: MONTH';
	current_min_value_text = extract(YEAR from lastpart_min_value) || '-' || extract(Month from lastpart_min_value)+1 || '-' || '01 00:00';
	current_min_value = to_timestamp(current_min_value_text,'YYYY-MM-DD HH24:MI');
	current_max_value_text = extract(YEAR from current_min_value) || '-' || extract(Month from current_min_value)+1 || '-' || '01 00:00';
	current_max_value = to_timestamp(current_max_value_text,'YYYY-MM-DD HH24:MI') - interval '1 second';
      	
      	RAISE NOTICE 'lastpart_min_value % , Curr_min_value %, Curr_max_value %',lastpart_min_value,current_min_value,current_max_value;


	--Postgres magic: 13 converts to next year!
	--current_min_value_text = '2017' || '-' || '15' || '-' || '01 00:00';
	--current_min_value = to_timestamp(current_min_value_text,'YYYY-MM-DD HH24:MI');
	--RAISE NOTICE 'current min value test 13 month: %',current_min_value;
      	
END
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
 
ALTER FUNCTION my_test(VARCHAR,VARCHAR, INT)
OWNER TO zabbix;

--test
SELECT my_test_date('history','1 week',4);


